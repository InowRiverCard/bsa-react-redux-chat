import React from 'react'
import { Icon } from 'semantic-ui-react'

import styles from './styles.module.css';


interface ChatHeaderProps {
  participants: number,
  messages: number,
  mostRecentDate: string
}

const ChatHeader: React.FC<ChatHeaderProps> = ({ participants, messages, mostRecentDate }) => (
    <div className={`${styles.header} segment`}>
        <span className={styles.title}>
            <Icon name='rocketchat' /> Teletonn
        </span>
        <div className={styles.info}>
            <span>{participants} {' '} participants</span>
            <span>{messages} {' '} messages</span>
        </div>
        <span className={styles.date}>last message at {' '} {mostRecentDate}</span>
    </div>
)

export default ChatHeader;
