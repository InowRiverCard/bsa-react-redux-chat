import React, { useState } from 'react';
import { Modal, Button } from 'semantic-ui-react';
import { Picker, Emoji } from 'emoji-mart'

import styles from './styles.module.css';
import IMessage from '../../interfaces/IMessage';

interface MsgEditModaleProps {
  message: IMessage,
  close(): void,
  editFunc(message: IMessage): void
}
const MsgEditModale: React.FC<MsgEditModaleProps> = ({ message, close, editFunc }) => {
  const [text, setText] = useState(message.text);
  const [piker, setPicker] = useState(false);

  const handleSave = () => {
    if (!text) {
      return;
    }
    const date = new Date().toUTCString();
    const updated = { ...message, text: text, editedAt: date };
    editFunc(updated);
    close();
  };

  const changeHandler = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(event.target.value);
  }

  const keyPressHandler = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter' && text.trim() !== '') {
      handleSave();
    }
  }

  const addEmoji = (e: any) => {
    setText(`${text}${e.native}`);
  }

  const toglePicker = () => {
    setPicker(!piker);
  } 

  return (
    <Modal open onClose={close} size="tiny" className={styles.modal} >
      <Modal.Header>
        <span>Edit Comment</span>
      </Modal.Header>
      <div className={styles.content} >
        <textarea
          rows={5}
          value={text}
          placeholder="Enter message"
          onChange={changeHandler}
          onKeyPress={keyPressHandler}
        />
        <div className={styles.smileBtn} onClick={toglePicker}>
          <Emoji emoji=":smiley:" size={16} />
        </div>
      </div>
      { piker &&
        <Picker
          native
          style={{ position: "absolute", left: "400px", bottom: "110px" }}
          onSelect={addEmoji} showSkinTones={false}
          emojiSize={16}
          showPreview={false}
        />
      }
      <Modal.Actions>
        <Button
          className={styles.btn}
          icon="save outline"
          floated="right"
          color="blue"
          onClick={handleSave}
          type="submit"
          disabled={text.trim() === ''}
          content="Save"
        />
        <Button className={styles.btn} floated="right" onClick={() => close()} basic>Cancel</Button>
      </Modal.Actions>
    </Modal>
  );
};

export default MsgEditModale;
