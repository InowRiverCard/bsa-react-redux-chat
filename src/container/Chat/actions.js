import * as messageService from '../../services/messageService';
import {
  SET_ALL_MESSAGES,
  ADD_MESSAGE,
  SET_EDITED_MESSEGE,
  SET_IS_LOADING
} from './actionTypes';

const setMessagesAction = messages => ({
  type: SET_ALL_MESSAGES,
  messages
});

const addMessageAction = message => ({
  type: ADD_MESSAGE,
  message
});

const setEditedMessageAction = message => ({
  type: SET_EDITED_MESSEGE,
  message
});

const setIsLoadingAction = isLoading => ({
  type: SET_IS_LOADING,
  isLoading
})

export const loadMessages = () => async dispatch => {
  const messages = await messageService.loadMessages();
  dispatch(setMessagesAction(messages));
  dispatch(setIsLoadingAction(false));
};

export const addMessage = message => async dispatch => {
  dispatch(addMessageAction(message));
}

export const updateMessage = message => async (dispatch, getRootState) => {
  const { messages: { messages } } = getRootState();
  const updMasseges = messages.map(msg => (msg.id === message.id ? message : msg));

  dispatch(setMessagesAction(updMasseges));
};

export const likeMessage = messageId => async (dispatch, getRootState) => {
  const updMessage = (msg) => {
    return msg.isLiked ? { ...msg, isLiked: false } : { ...msg, isLiked: true };
  }
  const { messages: { messages } } = getRootState();
  const updMasseges = messages.map(msg => (msg.id === messageId ? updMessage(msg) : msg));

  dispatch(setMessagesAction(updMasseges));
};

export const deleteMessage = messageId => async (dispatch, getRootState) => {
  const { messages: { messages } } = getRootState();
  const updMasseges = messages.filter(msg => (msg.id !== messageId));
  dispatch(setMessagesAction(updMasseges));
}

export const setEditedMessege = message => async dispatch => {
  dispatch(setEditedMessageAction(message));
}

export const setIsLoading = isLoading => async dispatch => {
  dispatch(setIsLoadingAction(isLoading));
}
