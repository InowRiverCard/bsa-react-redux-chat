import {
  SET_ALL_MESSAGES,
  ADD_MESSAGE,
  SET_EDITED_MESSEGE,
  SET_IS_LOADING
} from './actionTypes';
  
export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_MESSAGES:
      return {
        ...state,
        messages: action.messages,
      };
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.message],
      };
    case SET_EDITED_MESSEGE:
      return {
        ...state,
        editedMsg: action.message
      };
    case SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};