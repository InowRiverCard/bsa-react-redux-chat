import * as usersService from '../../services/usersService';
import { SET_USER } from './actionTypes';

const setUserAction = user => ({
  type: SET_USER,
  user
});

export const setUser = () => async dispatch => {
  const user = await usersService.getCurrentUser();
  dispatch(setUserAction(user));
};